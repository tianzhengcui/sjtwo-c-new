#include <stdio.h>

#include "FreeRTOS.h"
#include "task.h"

#include "board_io.h"
#include "periodic_callbacks.h"
#include "switch_led_logic.h"

/******************************************************************************
 * Your board will reset if the periodic function does not return within its deadline
 * For 1Hz, the function must return within 1000ms
 * For 1000Hz, the function must return within 1ms
 */

/*******************************************************************************
 *
 *                      Part 2: Switch and LED code module MODIFICATION
 *
 ******************************************************************************/

// Added switch_led_logic__initialize and switch_led_logic__run_once functions

// To enable "modules" for the code

void periodic_callbacks__initialize(void) {
  // This method is invoked once when the periodic tasks are created
  switch_led_logic__initialize(board_io__get_sw0());
}

void periodic_callbacks__1Hz(uint32_t callback_count) {

  gpio__toggle(board_io__get_led0());
  switch_led_logic__run_once(board_io__get_led0(), board_io__get_sw0());

  // /*******************************************************************************
  //  *
  //  *                      Part 3: Experiment with Task Overrun MODIFICATION
  //  *
  //  ******************************************************************************/
  //   // should use the following command in teminal:
  //   //
  //   // scons --no-unit-test
  //   //
  //   // otherwise, if only command "scons" is used, error will occurs

  //   if (callback_count >= 5) {
  //     vTaskDelay(1000);
  //   }
}

void periodic_callbacks__10Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led1());
  // Add your code here
}
void periodic_callbacks__100Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led2());
  // Add your code here
}

/**
 * @warning
 * This is a very fast 1ms task and care must be taken to use this
 * This may be disabled based on intialization of periodic_scheduler__initialize()
 */
void periodic_callbacks__1000Hz(uint32_t callback_count) {
  gpio__toggle(board_io__get_led3());
  // Add your code here
}